# production stage
FROM nginx:stable-alpine as production-stage
COPY dist /etc/nginx/html/
COPY default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
