import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import 'element-ui/lib/theme-chalk/index.css';
import ElementUI from 'element-ui';
import store from './store'
import permission from "./permission"
import VueCodeMirror from 'vue-codemirror'
import { basicSetup } from 'codemirror'
import 'codemirror/lib/codemirror.css'
//本地化
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
import v2Select from "components/v2-form/v2-select";
import v2Form from "components/v2-form/v2-form";
import V2Table from "components/table/V2Table";
import CodeEdit from "components/editor/CodeEdit";
import DialogForm from "components/form/DialogForm";
import TableForm from "components/table/TableForm.vue";
import DialogTable from "components/DialogTable.vue";
import asideMenu from "components/aside/aside-menu";
import BaseForm from "components/form/BaseForm";
import BaseTable from "components/table/BaseTable.vue";
import GpBaseTable from "components/gp-table/GpBaseTable.vue";
import GpBaseTableToolbar from "components/gp-table/GpBaseTableToolbar.vue";
import NumberColumn from "components/gp-table/NumberColumn.vue";
import BigNumberColumn from "components/gp-table/BigNumberColumn.vue";
import YiNumberColumn from "components/gp-table/YiNumberColumn.vue";
import TurnoverNumberColumn from "components/gp-table/TurnoverNumberColumn.vue";
import ShowGpPrice from "components/gp-table/ShowGpPrice.vue";

import TradeDateModel from "components/gp-table/TradeDateModel.vue";
import EditFormButton from "components/form/EditFormButton";
import DeleteButton from "components/form/DeleteButton";
import BaseTableAction from "components/table/BaseTableAction";
// 全局方法挂载
Vue.use(mavonEditor)
Vue.component("v2-select" , v2Select)
Vue.component("v2-form" , v2Form)
Vue.component("v2-table" , V2Table)
Vue.component("CodeEdit" , CodeEdit)
Vue.component("DialogForm" , DialogForm)
Vue.component("TableForm" , TableForm)
Vue.component("DialogTable" , DialogTable)
Vue.component("BaseForm" , BaseForm)
Vue.component("BaseTable" , BaseTable)
Vue.component("asideMenu",asideMenu)
Vue.component("EditFormButton",EditFormButton)
Vue.component("DeleteButton",DeleteButton)
Vue.component("BaseTableAction",BaseTableAction)
Vue.component("BaseTableAction",BaseTableAction)
Vue.component("GpBaseTable",GpBaseTable)
Vue.component("GpBaseTableToolbar",GpBaseTableToolbar)
Vue.component("TradeDateModel",TradeDateModel)
Vue.component("NumberColumn",NumberColumn)
Vue.component("BigNumberColumn",BigNumberColumn)
Vue.component("YiNumberColumn",YiNumberColumn)
Vue.component("TurnoverNumberColumn",TurnoverNumberColumn)
Vue.component("ShowGpPrice",ShowGpPrice)

Vue.use(VueCodeMirror,{
  autofocus: true,
  disabled: false,
  indentWithTab: true,
  tabSize: 2,
  extensions: [basicSetup]
})

Vue.config.productionTip = false

Vue.use(ElementUI)
new Vue({
  store ,
  router,
  render: h => h(App),
}).$mount('#app')
