import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import global from './modules/global'
import getters from './getters'
import learn from "./modules/learn";

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    user,
    global ,
    learn
  },
  getters
})

export default store
