import global from "@/store/modules/global";
import Admin from "@/router/modules/admin";

const getters = {
  sidebar: state => state.app.sidebar,
  size: state => state.app.size,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  introduction: state => state.user.introduction,
  roles: state => state.user.roles,
  permissions: state => state.user.permissions,
  permission_routes: state => state.permission.routes,
  topbarRouters:state => state.permission.topbarRouters,
  defaultRoutes:state => state.permission.defaultRoutes,
  sidebarRouters:state => state.permission.sidebarRouters,
  book: state => state.learn.book,
  chapter: state => state.learn.chapter,
  bookActiveId: state => state.learn.bookActiveId ,
  bookActiveIndex: state => state.learn.bookActiveIndex ,
  learnHomeActiveId: state => state.learn.learnHomeActiveId ,
  site: state => state.learn.site ,
  isCollapse: state => state.global.isCollapse ,
  userInfo: state => state.user.userInfo ,
  keyCollapse: (state, getters) => getters.screen > 1 ? getters.isCollapse : false,
  screen: state => state.global.screen,
  menu: (state) => {
    return state.user.menu
  },
  menuAll:(state)=>{
    return state.user.menuAll
  },
  isLock: state => state.global.isLock,
  website: state => state.global.website,

}
export default getters
