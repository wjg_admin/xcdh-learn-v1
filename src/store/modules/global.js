import Cookies from 'js-cookie'
import website from "@/config/website";
import util from "utils/util";
import router from "@/router";
const state = {
    isCollapse: get("isCollapse") === "true" ,
    screen: -1 ,
    isLock: false ,
    website: website,
    menuActive: get("menuActive") ,
    tabs: getArray("tabs"),
    gpActive: get("gpActive")
}

function getObj(key , ){
   let val = get(key)
    if(val != null){
        return JSON.parse(val)
    }
    return  {}
}
function getArray(key){
    let val = get(key)
    if(val != null){
        return JSON.parse(val)
    }
    return  []
}

function get(key){
    return localStorage.getItem(key)
}
function set(key ,val){
    if(typeof val == "object"){
        localStorage.setItem(key , JSON.stringify(val))
    }else{
        localStorage.setItem(key , val)
    }

}
const mutations = {
    setCollapsed(state , val){
        set("isCollapse", val)
        state.isCollapse = val ;
    },
    SET_MENU_ACTIVE(state , val){
        set("menuActive", val)
        state.menuActive = val ;
    },
    SET_GP_ACTIVE(state , val){
        set("gpActive", val)
        state.gpActive = val ;
    },
    SET_TABS(state,tab){
        let index = state.tabs.findIndex(t=>t.path === tab.path ) ;
        if(index === -1 && tab.path !== "/admin/home"){
            state.tabs.push(tab);
            set("tabs", state.tabs)
        }
    },
    REMOVE_TABS(state , path ){
        let index = state.tabs.findIndex(t=>t.path === path) ;
        if(index !== -1){
            state.tabs.splice(index, 1) ;
            if(state.tabs.length === 0){
                router.push(website.adminHone.path)
            }
            set("tabs", state.tabs)
        }
    }
}

const actions = {
}
const getters = {
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}
