import {getCourseType} from 'api/student'

export default {
    state:{
        courseTypeList:[]
    },
    mutations:{
        setcourseList(state , list){
            state.courseTypeList = list
        }
    },
    getters: {
        courseTypeList(state){
            return state.courseTypeList ;
        }
    },
    actions:{
        getCourseType(content ,payload){
            if(content.state.courseTypeList.length == 0){
                getCourseType(payload).then(res=>{
                    res.execute(data=>{
                        content.commit("setcourseList",data.result.data)
                    })
                })
            }
            
        }
    }
}