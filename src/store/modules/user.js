
import webiste from '@/config/website'
import {loginByUsername,  getUserInfo, getMenu, getTopMenu, logout, refreshToken, getButtons} from '@/api/auth/user'
import util from "utils/util";
import store from "utils/store"
import {removeToken, setToken} from "utils/auth";


const user = {
  state: {
    userInfo: util.NVL(store.getStore({name: 'userInfo'}),[]),
    permission: util.NVL(store.getStore({name: 'permission'}),{}),
    roles: [],
    menu: util.NVL(store.getStore({name: 'menu'}),[]),
    menuAll: util.NVL(store.getStore({name: 'menuAll'}),[]),
    token: util.NVL(store.getStore({name: 'token'}),""),
  },
  actions: {
    //根据用户名登录
    LoginByUsername({commit}, userInfo) {
      return new Promise((resolve, reject) => {
        loginByUsername( userInfo.username, userInfo.password, userInfo.type).then(res => {
          const data = res.data;
          commit('SET_TOKEN', data.token);
          commit('SET_USER_INFO', data);
          commit('DEL_ALL_TAG');
          commit('CLEAR_LOCK');
          resolve(res);
        }).catch(error => {
          reject(error);
        })
      })
    },
    //根据手机号登录
    LoginByPhone({commit}, userInfo) {
      return new Promise((resolve) => {
        loginByUsername(userInfo.phone, userInfo.code).then(res => {
          const data = res.data.data;
          commit('SET_TOKEN', data);
          commit('DEL_ALL_TAG');
          commit('CLEAR_LOCK');
          resolve();
        })
      })
    },
    GetUserInfo({commit}) {
      return new Promise((resolve, reject) => {
        getUserInfo().then((res) => {
          const data = res.data.data;
          commit('SET_ROLES', data.roles);
          resolve(data);
        }).catch(err => {
          reject(err);
        })
      })
    },
    //刷新token
    RefreshToken({state, commit}) {
      return new Promise((resolve, reject) => {
        refreshToken(state.refreshToken).then(res => {
          const data = res.data.data;
          commit('SET_TOKEN', data);
          resolve(data);
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 登出
    LogOut({commit}) {
      return new Promise((resolve, reject) => {
        logout().then(() => {
          commit('SET_TOKEN', '');
          commit('SET_MENU', [])
          commit('SET_MENU_ALL', []);
          commit('SET_ROLES', []);
          commit('DEL_ALL_TAG');
          commit('CLEAR_LOCK');
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },
    //注销session
    FedLogOut({commit}) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '');
        commit('SET_MENU', []);
        commit('SET_MENU_ALL', []);
        //commit('SET_ROLES', []);
        //commit('DEL_ALL_TAG');
        removeToken()
        resolve()
      })
    },
    GetTopMenu() {
      return new Promise(resolve => {
        getTopMenu().then((res) => {
          const data = res.data.data || []
          resolve(data)
        })
      })
    },
    //获取系统菜单
    GetMenu({commit, dispatch}, parentId) {
      return new Promise(resolve => {
        getMenu(parentId).then((res) => {
          const menu = res.data.menuTree ;
          const sources = res.data.sources ;
          commit('SET_MENU', menu);
          commit('SET_MENU_ALL', sources);
          resolve(menu)
        })
      })
    },
    GetButtons({commit}) {
      return new Promise((resolve) => {
        getButtons().then(res => {
          const data = res.data.data;
          commit('SET_PERMISSION', data);
          resolve();
        })
      })
    },
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token;
      setToken(token)
    },
    SET_USER_INFO: (state, userInfo) => {
      state.userInfo = userInfo;
      store.setStore({name: 'userInfo', content: state.userInfo})
    },
    SET_MENU_ALL: (state, menuAll) => {
      state.menuAll = menuAll
      store.setStore({name: 'menuAll', content: state.menuAll, type: 'session'})
    },
    SET_MENU: (state, menu) => {
      state.menu = menu
      store.setStore({name: 'menu', content: state.menu, type: 'session'})
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles;
    },
    SET_PERMISSION: (state, permission) => {
      let result = [];

      function getCode(list) {
        list.forEach(ele => {
          if (typeof (ele) === 'object') {
            const children = ele.children;
            const code = ele.code;
            if (children) {
              getCode(children)
            } else {
              result.push(code);
            }
          }

        })
      }

      getCode(permission);
      state.permission = {};
      result.forEach(ele => {
        state.permission[ele] = true;
      });
      store.setStore({name: 'permission', content: state.permission, type: 'session'})
    }
  }

}
export default user
