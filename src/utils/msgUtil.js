import {MessageBox } from 'element-ui'
export default {
    delConfirm(){
        let array = []
         if(arguments.length === 1){
            if(typeof(arguments[0]) === "string"){
                array[0] = arguments[0] ;
            }else if(typeof(arguments[0]) === "function"){
                array[1] = arguments[0] ;
            }
        }else if(arguments.length === 2){
             if(typeof(arguments[0]) === "string"){
                 array[0] = arguments[0] ;
             }
             if(typeof(arguments[0]) === "function"){
                 array[1] = arguments[0] ;
             }
             if(typeof(arguments[1]) === "function"){
                 array[1] = arguments[1] ;
             }
         }else{
             array = arguments ;
         }

        MessageBox({
            message: array[0] || "操作提示" ,
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            showCancelButton: true ,
            type: 'warning'
        }).then((res)=>{
            if(typeof array[1] === "function"){
                array[1](res)
            }
        }).catch((error)=>{
            if(typeof array[2] === "function"){
                array[1](error)
            }
        })
    }
}
