
const toString = Object.prototype.toString ;
class Is{
    constructor(a,b) {
        this.is = a===b ;
    }
    get(a,b){
        return this.is ? a: b
    }
}
export default {
    is(a ,b){
       return new Is(a,b)
    },
    isEmpty(t){
       if(this.isNull(t) || this.isUndef(t)){
           return true
       }
       if(this.isString(t) || this.isArray(t)){
           return t.length === 0
       }
       if(this.isObj(t)){
           return JSON.stringify(t) === "{}"
       }
       return false ;
    },
    isUndef(t) {
       return typeof t === "undefined"
    },
    isNull(t){
        return t === null ;
    },
    isVoid(a){
        return [undefined ,null , ""].includes(a)
    },
    isObj(t){
       return toString.call(t) === "[object Object]"
    },
    isBlank(t){
        if(this.isString(t)){
            return t.length === 0
        }
        return false ;
    },
    isString(t){
        return typeof t === "string"
    },
    trim(t){
        if(this.isString(t)){
            return t.replace(/\s+/g , "")
        }
        return t;
    },
    isArray(t) {
        return Array.isArray(t);
    },
    copy(t){
        return JSON.parse(JSON.stringify(t))
    },
    forEach(d,c){
        if(this.isArray(d)){
            d.forEach(c)
        }else if(this.isObj(d)){
            for(let k in d){
              let res = c(d[k],k)
              if(res === true) return ;
            }
        }else {
            let s = d + '' ;
            s.split("").forEach(c) ;
        }
    },
    NVL(a,b){
        return this.isVoid(a) ? b :a
    },
    nvl(a,b){
        return this.NVL(a,b) ;
    },
    isBool(t){
        //isBool
        return toString.call(t) === "[object Boolean]"
    },
    isTrue(t){
        return t === true
    },
    isFun(t){
        return toString.call(t) === "[object Function]"
    },
    isPromise(t) {
        return toString.call(t) === "[object Promise]"
    },
    ifTruthy(a , b, c){
        return a ? b : c ;
    },
    diff(obj1, obj2){
        delete obj1.close;
        var o1 = obj1 instanceof Object;
        var o2 = obj2 instanceof Object;
        if (!o1 || !o2) { /*  判断不是对象  */
            return obj1 === obj2;
        }

        if (Object.keys(obj1).length !== Object.keys(obj2).length) {
            return false;
            //Object.keys() 返回一个由对象的自身可枚举属性(key值)组成的数组,例如：数组返回下表：let arr = ["a", "b", "c"];console.log(Object.keys(arr))->0,1,2;
        }

        for (var attr in obj1) {
            var t1 = obj1[attr] instanceof Object;
            var t2 = obj2[attr] instanceof Object;
            if (t1 && t2) {
                return this.diff(obj1[attr], obj2[attr]);
            } else if (obj1[attr] !== obj2[attr]) {
                return false;
            }
        }
        return true;
    },
    format() {
        const args = arguments;
        if (args.length === 0) return "";
        let template = args[0] , i = 1;
        return template.replace(/%(-?\d*)?s/g, function(match, p1) {
            let padding = '',
                target = args[i] || '';
            if (p1) {
                let num = parseInt(p1);
                let handler = target
                if(this.isString(target)){
                    handler = {value: target , repeat: ' '}
                }
                let fn = this.ifTruthy(num < 0 , "".padEnd , "".padStart)
                padding = fn(Math.abs(num) , handler.repeat)
            }
            i++;
            return padding;
        });
    },
    downLoad(blob , filename = `${Date.now}`){
        // 创建一个临时的 URL 对象
        const urlObject = window.URL.createObjectURL(blob);
        // 创建一个 <a> 标签
        const linkElement = document.createElement('a');
        linkElement.href = urlObject;
        linkElement.download = filename;
        // 模拟点击下载
        linkElement.click();
    }
}
