import Cookies from 'js-cookie'


export default {
    setCourseId( courseId) {
        return Cookies.set("courseId" , courseId)
    },
    getCourseId(){
        return Cookies.get("courseId")
    },
    removeCourseId(){
        return Cookies.remove("courseId")
    }
}
