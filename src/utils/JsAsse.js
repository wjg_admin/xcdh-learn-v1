import commons from "utils/commons";

export default {
    isTrue(state,msg){
        if(!state){
            throw msg
        }
    },
    isNotEmpty(target,msg){
        if(commons.isEmpty(target)){
            throw msg
        }
    },
    isFun(target,msg){
        if(!commons.isFun(target)){
            throw msg
        }
    }
}