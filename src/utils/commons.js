import REG from "./reg"
const toString = Object.prototype.toString ;
/**
     * 处理数组验证
     * @param {*} target
     * @param {*} reslove
     * @param {*} reject
     * @returns
     */
 function handlerArrayValiData(target ,reslove,reject ){
    for(let item of target){
        let expr = item.expr;
        if(this.isString(expr)){
            expr = REG[expr] ;
        }
        if(this.isBool(expr)){
            if(!expr){
                return reject({
                    msg: item.msg ,
                    status: false
                })
            }
        }else  if(this.isTrue(expr)){
            if (this.isRegExp(expr)) {
                if(!expr.test(item.value)){
                    return reject({
                        msg: item.msg ,
                        status: false
                    })
                }
            }else if(this.isFun(expr)){
                let res = expr(item) ;
                if(!this.isTrue(res)){
                        return reject({
                            msg: item.msg ,
                            status: false
                        })
                }
            }else{
                console.warn("没有合适的比较!!!")
                return reject({
                    msg: item.msg ,
                    status: false
                })
            }
        }else{
            if(this.isEmpty(item.value)){
                return reject({
                    msg: item.msg ,
                    status: false
                })
            }
        }
    }
    return reslove({
        msg: '验证通过',
        status: true
    })
}

/**
 * 处理对象验证
 * @param {*} param0
 * @param {*} reslove
 * @param {*} reject
 */
function handlerObjValiData({data = {}, target = {}} ,  reslove , reject){
    for(let key in target){
        if(this.isEmpty(data[key])){
           return reject({
                msg: target[key],
                status: false
            })
        }
    }
    return reslove({
        msg: '验证通过',
        status: true
    })
}

export default {
    /**
     * 参数是数组或者对象
     * @param {*} target
     * @returns
     */
    validata(target){
        // true : 成功 , false : 失败
        return new Promise((reslove , reject)=>{
            if(this.isArray(target)){
                handlerArrayValiData.call( this, target, reslove , reject)
            }else if(this.isObj(target)){
                handlerObjValiData.call(this , target ,reslove , reject ) ;
            }else {
                throw new Error("参数是数组[{msg: '' , value: '' , expr: ''}]或者对象{}")
            }
        })
    },

    /**
     *
     * @param data { [{
     *     value: '' ,
     *     msg: '',
     *     fun: Function
     * }] }
     */
    simpleVData(data){
        for(let index in data){
            let item = data[index]
            if(this.isVoid(item.value)){
                return {
                    msg: item.msg,
                    status: false
                }
            }
        }
        return {
            msg: "",
            status: true
        }
    },
    //只比较字符串
    isBlank(target){
        if(this.isValid(target) && this.isString()){
           return this.trim(target).length === 0 ;
        }
        return false ;
    },
    /**
     * 去除空格
     * @param {\} target
     * @returns
     */
    trim(target){
        if(this.isString(target)){
            return target.replace(/\s*/g,"");
        }
        return target ;
    },
    /**
     * 是否为空
     * @param {*} target
     * @returns
     */
    isEmpty(target){
        if(this.isString(target)){
            return target.length === 0;
        }else if(this.isObj(target)){
            return Object.keys(target).length === 0 ;
        }else if(this.isArray(target)){
            return target.length === 0 ;
        }
        return (target === null  ||  target === undefined)
    },
    isString(target){
        return toString.call(target) === "[object String]"
    },
    isUndef(target){
        return toString.call(target) === "[object Undefined]"
    },
    isNull(target){
        return toString.call(target) === "[object Null]"
    },
    /**
     * 有效
     * @param {*} target
     * @returns
     */
    isValid(target){
        return (!this.isNull(target) && !this.isUndef(target))
    },
    isVoid(target){
      return ['',null,undefined].includes(target)
    },
    nvl(a,b){
        return this.isVoid(a) ? b :a
    },
    //[object RegExp]
    isRegExp(target){
        return toString.call(target) === "[object RegExp]"
    },
    isObj(target){
        return toString.call(target) === "[object Object]"
    },
    isArray(target){
        return toString.call(target) === "[object Array]"
    },
    isTrue(target){
        return target === true
    },
    isFun(target){
        return toString.call(target) === "[object Function]"
    },
    isBool(target){
        //isBool
        return toString.call(target) === "[object Boolean]"
    }

}
