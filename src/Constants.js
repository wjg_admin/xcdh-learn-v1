export default {
    menuActive: 'global/menuActive',
    setMenuActive: 'global/SET_MENU_ACTIVE' ,
    setGpActive: "global/SET_GP_ACTIVE" ,
    setCourseId: "global/setCourseId" ,
    setBook: "learn/setBook" ,
    setBookActiveId: "learn/setBookActiveId" ,
    bookActiveIndex: "learn/setBookActiveIndex" ,
    learnCtSidebarRefresh: "learn/setLearnCtSidebarRefresh" ,
    setLearnHomeActiveId: "learn/setLearnHomeActiveId" ,
    setSite: 'learn/setSite'
}
