import GpIndex from "@/views/stock/index";

const Stock = [
    {
        path: '/gp' ,
        name: '股票首页' ,
        isHome: true,
        icon: 'el-icon-receiving',
        component: GpIndex,
        redirect: "/gp/policy",
        hideAside: true ,
        index: "0-0",
        children: [
            {
                path: "/gp/policy",
                icon: 'el-icon-cpu',
                name: '政策/利好',
                component: ()=> import("@/views/stock/stock/policy"),
            },
            {
                path: "/gp/news",
                name: '新闻',
                component: ()=> import("@/views/stock/stock/news"),
            },
            {
                path: "/gp/golpe",
                name: '打板',
                component: ()=> import("@/views/stock/stock/golpe"),
            },

            {
                path: "/gp/capital/flow/hotMoney",
                name: '游资交易',
                component: ()=> import("@/views/stock/stock/HotMoney"),
            },
            {
                path: "/gp/capital/flow/stock",
                name: '个股资金流向',
                component: ()=> import("@/views/stock/stock/capitalFlowStock"),
            },
            {
                path: "/gp/capital/flow/sector",
                name: '板块资金流向',
                component: ()=> import("@/views/stock/stock/capitalFlowSector"),
            },
            {
                path: "/gp/capital/flow/industry",
                name: '行业资金流向',
                component: ()=> import("@/views/stock/stock/capitalFlowIndustry"),
            },
            {
                path: "/gp/day/dailyIndicators",
                name: '每日指标',
                component: ()=> import("@/views/stock/stock/DailyIndicators"),
            },
            {
                name: '股票列表',
                path: '/gp/aStock' ,
                component: ()=> import("@/views/stock/stock/AStock"),
            },
            {
                path: "/gp/stockBidding",
                name: '今日竞价',
                component: ()=> import("@/views/stock/stock/StockBidding"),
            },
            {
                path: "/gp/gpHistoryName",
                name: '今日竞价',
                component: ()=> import("@/views/stock/stock/GpHistoryName"),
            },
            {
                path: "/gp/day/trDetail",
                name: '今日竞价',
                component: ()=> import("@/views/stock/stock/trDetail"),
            }
        ]
    }

]
export default Stock
