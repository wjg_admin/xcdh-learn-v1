const UpdateSection = ()=> import('@/views/learn/content/section/UpdateSection')
const Log = ()=>import("@/views/learn/log/index")
const learnRouter = [
    {
        path:'/',
        hidden: true ,
        redirect: "/v2/learn"
    },
    {
        path:'/siteTool',
        component: ()=> import("@/views/learn/site/site-tool"),
    },

    {
        path: '/v2/learn' ,
        component: ()=> import("@/views/learn/learnHome"),
        redirect: "/site",
        children: [
            {
                path: "/book",
                component: ()=> import("@/views/learn/book"),
            },
            {
                path: '/site' ,
                component: ()=> import("@/views/learn/site"),
            },
            {
                path: '/log',
                component: Log
            },
        ]
    },
    {
        path: '/v2/content' ,
        component: ()=> import("views/learn/content/contentLayout"),
        redirect: "/chapter",
        children: [
            {
                path: "/section/:id",
                component: ()=> import("@/views/learn/content/section/index"),
            },
            {
                path: '/updateSection',
                name: 'updateSection',
                component: UpdateSection
            },
            {
                path: "/chapter",
                component: ()=> import("@/views/learn/content/chapter"),
            }
        ]
    }

]
export default learnRouter
