import AdminLayout from "@/pages/layout/AdminLayout";

const Admin = [
    {
        path: '/home' ,
        name: '首页' ,
        isHome: true,
        icon: 'el-icon-receiving',
        component: AdminLayout,
        redirect: "/admin/home",
        hideAside: true ,
        index: "0-0",
        children: [
            {
                path: "/admin/home",
                icon: 'el-icon-cpu',
                name: '首页',
                index: "0-0",
                component: ()=> import("@/views/admin/Home"),
            }
        ]
    },
    {
        name: '基础数据' ,
        icon: 'el-icon-receiving',
        component: AdminLayout,
        redirect: "/admin/home",
        index: "1",
        children: [
            {
                path: "/admin/baseData",
                icon: 'el-icon-cpu',
                name: '字典管理',
                index: "1-0",
                component: ()=> import("@/views/admin/ds/toolDict"),
            }
        ]
    },
    {
        path: '/admin' ,
        icon: 'el-icon-setting',
        component: AdminLayout,
        name: 'IT工具' ,
        redirect: "/admin/home",
        index: "2",
        children: [
            {
                path: "/it_excel",
                icon: 'el-icon-cpu',
                name: 'excel文件输入',
                component: ()=> import("@/views/admin/it/ITExcel"),
                index: "2-0",
            },
            {
                path: "/it_ds",
                icon: 'el-icon-cpu',
                name: "数据源管理",
                index: "2-1",
                component: ()=> import("@/views/admin/ds/DataSource"),
            },
            {
                path: "/it_tpl",
                icon: 'el-icon-cpu',
                name: "模板管理",
                index: "2-2",
                component: ()=> import("@/views/admin/ds/ITProject.vue"),
            },
            {
                path: "/it_code",
                icon: 'el-icon-cpu',
                name: "代码生成器",
                index: "2-3",
                component: ()=> import("@/views/admin/it/ITCode"),
            }
        ] ,
    }, //el-icon-s-platform
    {
        path: '/viewCode' ,
        name: '可视化开发' ,
        icon: 'el-icon-s-platform',
        component: AdminLayout,
        redirect: "/viewCode/viewCode",
        index: "3",
        children: [
            {
                path: "/viewCode/viewCode",
                icon: 'el-icon-cpu',
                name: '按钮管理',
                index: "3-0",
                component: ()=> import("@/views/view-code/Button"),
            }
        ]
    }

]
export default Admin
