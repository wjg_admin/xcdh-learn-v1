import util from "utils/util";
import AdminLayout from "@/pages/layout/AdminLayout";
import router from './index'
import store from "@/store";
//
export default   {
    menuToRouter(menus = []){
        let routers = []
        menus.forEach(menu=>{
            let route = util.copy(menu)
            if(!menu.leaf){//并不是叶子节点
                route.component = AdminLayout ;
                this.handlerDeep2Menu(route.children)
            }else{
                route.component = ()=> import(`@/views${route.path}`)
            }
            routers.push(route)
        })
        return routers
    },
   getAdminMenus(){
       return this.menuToRouter(store.state.user.menu)
   },
    isLoadAdminMenu(){
        return store.state.user.menu.length === 0
    },
   handlerDeep2Menu(children){
        children.forEach(menu=>{
            if(menu.leaf){
                this.handlerComponent(menu)
            }else{
                this.handlerDeep2Menu(menu.children)
            }
        })
    },
   handlerComponent(menu){
        if(menu.path){
            if(menu.path.startsWith("/")){
                menu.component = ()=> import(`@/views${menu.path}`)
                return
            }
            menu.component = ()=> import(`@/views/${menu.path}`)
        }
   },
   addRoutes(menus){
       router.matcher.addRoutes(this.menuToRouter(menus))
   }
}
