import Vue from 'vue'
import VueRouter from 'vue-router'
import learnRouter from "./modules/learn";
import mhtw from "@/router/modules/mhtw";
import Chat from "@/router/chatGpt";
import Stock from "@/router/stock";
import AdminLayout from "@/pages/layout/AdminLayout";
import RouterUtil from "@/router/RouterUtil";

const Index = () => import('../views/auth/login');
const Register = () => import('../views/auth/Register');



Vue.use(VueRouter)

export const constantRoutes =
    [
        {
            path:'*',
            name: '*' ,
            hidden: true ,
            component: ()=> import("../views/error/404"),
        },
        {
            path:'/login',
            name: 'login' ,
            hidden: true ,
            component: Index,
            meta:{
                authen: false ,
            }
        },
        {
            path:'/register',
            hidden: true ,
            component: Register,
            meta:{
                authen: false ,
            }
        },
        ...learnRouter ,
        ...Chat ,
        ...mhtw ,
        ...Stock ,
        {
            path: '/home' ,
            name: '首页' ,
            isHome: true,
            icon: 'el-icon-receiving',
            component: AdminLayout,
            redirect: "/admin/home",
            hideAside: true ,
            index: "0-0",
            children: [
                {
                    path: "/admin/home",
                    icon: 'el-icon-cpu',
                    name: '首页',
                    index: "0-0",
                    component: ()=> import("@/views/admin/Home"),
                }
            ]
        }

]


const createRouter = (router=[]) => {
    return new VueRouter({
        mode: "history",
        //mode: 'hash',
        scrollBehavior: () => ({ y: 0 }),
        routes: router
    })
}

const router = createRouter(constantRoutes.concat(RouterUtil.getAdminMenus()))
export default router
