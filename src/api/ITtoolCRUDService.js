import http from "network/http";
import Service from "api/Service";

class ITtoolCRUDService{
    constructor(opts) {
        this.moudle = opts.moudle
    }
    update(data){
        return http({
            url: `/${Service.itToolService}/${this.moudle}/update`,
            method: 'POST',
            data: data
        })
    }
    updateBatch(list){
        return http({
            url: `/${Service.itToolService}/${this.moudle}/updateBatch`,
            method: 'POST',
            data: list
        })
    }
    save(data){
        return http({
            url: `/${Service.itToolService}/${this.moudle}/save`,
            method: 'POST',
            data: data
        })
    }
    saveBatch(list){
        return http({
            url: `/${Service.itToolService}/${this.moudle}/saveBatch`,
            method: 'POST',
            data: list
        })
    }
    deleteByIds(ids){
        return http({
            url: `/${Service.itToolService}/${this.moudle}/deleteByIds`,
            method: 'POST',
            data: ids
        })
    }
    listByPage(data){
        return http({
            url: `/${Service.itToolService}/${this.moudle}/listByPage`,
            method: 'POST',
            data: data
        })
    }
    queryList(data){
        return http({
            url: `/${Service.itToolService}/${this.moudle}/queryList`,
            method: 'POST',
            data: data
        })
    }

}
export default ITtoolCRUDService