import http from "network/http";
import Service from "api/Service";

export const menuTree = (data)=>{
    return http({
        url: `${Service.xcdhBaseService}/menu/menuList` ,
        method: 'POST' ,
        data:data
    })
}

export const asideMenuTree = (data)=>{
    return http({
        url: `${Service.xcdhBaseService}/menu/asideMenuTree` ,
        method: 'POST' ,
        data:data
    })
}



