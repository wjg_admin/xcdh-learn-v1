import http from "network/http";
import Service from "../../Service";
export default  {
    upload(formData){
        return http({
            url: `/${Service.learnService}/oss/uploadImage` ,
            method: "POST",
            data: formData ,
            timeout: -1
        })
    },
    delete(fileName){
        return http({
            url: `/${Service.learnService}/learn/oss/delete` ,
            method: "POST",
            data: "fileName="+fileName
        })
    }
}
