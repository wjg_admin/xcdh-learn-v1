import util  from "utils/util";
import ApiService from "api/ApiService";

export default class ApiPageService{
    constructor(apiService) {
        if(util.isEmpty(apiService) || apiService.constructor !== ApiService){
            throw new Error("类型错误")
        }
        this.apiService = apiService ;
    }
    list(current, page ,params){
        return this.apiService.getList(current, page ,params)
    }
    refresh(){

    }
    computed(){
        let that = this ;
        return [
           function current(val){
               that.list(val , this.page , this.params)
            }
        ]
    }
}
