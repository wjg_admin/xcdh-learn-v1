import http from "network/http";
import Service from "api/Service";
const baseUrl = `` ;


export const loginByUsername = ( account, password, type, ) => {
    return http.post(`${Service.authService}/auth/login` , {
        username: account ,
        password: password
    })
}

export const getButtons = () => http({
    url: '/api/blade-system/menu/buttons',
    method: 'get'
});

export const getUserInfo = () => http({
    url: baseUrl + '/user/getUserInfo',
    method: 'get'
});

export const refreshToken = () => http({
    url: baseUrl + '/user/refesh',
    method: 'post'
})

export const registerGuest = (form, oauthId) => http({
    url: '/api/blade-user/register-guest',
    method: 'post',
    params: {
        tenantId: form.tenantId,
        name: form.name,
        account: form.account,
        password: form.password,
        oauthId
    }
});

export const getMenu = () => http({
    url: '/xcdhBaseService/menu/getMenuTree',
    method: 'get'
});

export const getCaptcha = () => http({
    url: '/api/blade-auth/captcha',
    method: 'get'
});

export const getTopMenu = () => http({
    url: baseUrl + '/user/getTopMenu',
    method: 'get'
});
export const logout = () => http({
    url: baseUrl + '/user/logout',
    method: 'get'
})
