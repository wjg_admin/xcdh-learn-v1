import ApiConfig from "api/ApiConfig";
import http from "network/http";
import Service from "./Service";
export default class ApiService{
    constructor(prefix , curd) {
        this.prefix = this.handlerPrefix(prefix) ;
        this.crud = curd || ApiConfig.curd ;
    }
    getList(current, size, params = {}){
        return http({
            url: `${this.prefix}${this.crud.queryAll}`,
            method: 'get',
            params: {
                ...params,
                current,
                size,
            }
        })
    }
    getDetail(id){
        return http({
            url: `${this.prefix}${this.crud.query}`,
            method: 'get',
            params: {
                id
            }
        })
    }
    remove(ids){
        return http({
            url: `${this.prefix}${this.crud.del}`,
            method: 'post',
            params: {
                ids,
            }
        })
    }
    update(row){
        return http({
            url: `${this.prefix}${this.crud.update}`,
            method: 'post',
            data: row
        })
    }
    add(row){
        return http({
            url:  `${this.prefix}${this.crud.add}`,
            method: 'post',
            data: row
        })
    }
    handlerPrefix(prefix){
        if(!prefix.startsWith("/")){
            prefix = `/${prefix}`
        }
        if(!prefix.endsWith("/")){
            prefix = `${prefix}/`
        }
        return `/${Service.learnService}${prefix}`
    }

}
