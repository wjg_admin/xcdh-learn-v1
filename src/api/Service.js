const Service = {
    learnService: 'xcdhLearnService' ,
    itToolService: 'xcdhItTools' ,
    authService: 'xcdhAuth',
    xcdhLowCode: "xcdhLowCode" ,
    xcdhBaseService: "xcdhBaseService"
}
export default Service ;
