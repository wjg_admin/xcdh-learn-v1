import http from "network/http";
import Service from "../Service";
export default {
    completion(text){
        return http({
            url: `/${Service.learnService}/chat/completion`,
            method: 'post',
            meta:{from: true} ,
            data: {
                text
            }
        })
    },
    dialogue(text){
        return http({
            url: `/${Service.learnService}/chat/dialogue`,
            method: 'post',
            meta:{from: true} ,
            data: {
                text
            }
        })
    }
}
