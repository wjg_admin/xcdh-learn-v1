import ApiService from "api/ApiService";
import http from "network/http";
const api = new ApiService("contentex") ;
api.getList = function (params){
    return http({
        url: `${this.prefix}${this.crud.queryAll}`,
        method: 'get',
        params: params
    })
}
api.detail = function (id){
    return http({
        url: `${this.prefix}detail`,
        method: 'get',
        params: {id}
    })
}
export  default  api
