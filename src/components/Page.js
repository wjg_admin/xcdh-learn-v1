import commons from "utils/commons";

export default class Page{
    constructor(props = {}) {
       this.current = commons.nvl(props.current,1)
       this.size = commons.nvl(props.size,10);
       this.total = 0
    }
    addSize(number){
        this.size += number
    }
    incr(){
        this.current += 1
    }
    decr(){
        if(this.current>0){
            this.current -= 1
        }
    }
    jump(number){
        if(number>0){
            this.current = number
        }
    }

}