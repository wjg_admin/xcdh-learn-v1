import JsAsse from "utils/JsAsse";
import commons from "utils/commons";

export default class TableFormColumns {
    constructor(config) {
        this.checkToken(config) ;
        this.columns = config.columns ;
        this.tfc = this.getArray(config.tfc );
        this.ffc = this.getArray(config.ffc ) ;
    }
    checkToken(config){
        let {columns} = config
        JsAsse.isTrue(Array.isArray(columns) && columns.length > 0, '配置项config.columns不可为空');
    }
    getArray(array){
        return Array.isArray(array) ? array : []
    }
    getTableColumns(){
        return this.columns.filter(col=>{
            return !this.tfc.includes(col.key)
        }).filter(col=>{
            if(commons.isFun(col.show)){
                return col.show() === true
            }
            return commons.nvl(col.show, true)
        })
    }
    getFormColumns(){
        return this.columns.filter(col=>{
            return !this.ffc.includes(col.key)
        }).filter(col=>{
            return col.type !== 'buttons'
        }).filter(col=>{
            if(commons.isFun(col.show)){
                return col.show() === true
            }
            return commons.nvl(col.show, true)
        })
    }
}