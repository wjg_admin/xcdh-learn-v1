import commons from "utils/commons";

export default class TableHead{
    constructor(config) {
        this.headTableForm(config)
        this.buttons = this.getBtn(config.buttons)
    }
    getBtn(buttons = []){
        if(this.searchColumns.length > 0){
           let index = buttons.findIndex(item=>item.type === "search") ;
           if(index == -1){
               return this.getSearchBtn().concat(buttons)
           }
        }
        return buttons
    }
    getSearchBtn(){
        return [
            {
                title: '搜索',
                type: 'search'
            }
        ]
    }
    headTableForm(config){
        let {columns} = config
        let searchColumns = columns.filter((col)=>{
            return col.search === true
        })
        this.searchColumns = searchColumns
    }
}