import JsAsse from "utils/JsAsse";
import commons from "utils/commons";
import TableFormColumns from "components/table/TableFormColumns";
import TableHead from "components/table/TableHead";
import Page from "components/Page";
import http from "network/http";

export default class TableForm{
    constructor(config) {
        this.checkToken(config) ;
        this.showPage= commons.nvl(config.showPage,true) ; // 是否显示分页
        this.params = config.params ;// 请求列表参数
        this.url = config.url ;// 请求路径
        this.method = commons.nvl(config.method,'get');//请求方式
        this.lazy = commons.nvl(config.lazy,false) ;// 是否首次不加载
        this.columns = new TableFormColumns(config)
        this.tableHead = new TableHead(config) ;
        this.page = new Page(config.page)
        this.pageAlign = commons.nvl(config.pageAlign,'right') ;//
        this.isTree = commons.nvl(config.isTree,false) ;//
        this.rowKey = config.rowKey ;//
        this.formInline = commons.nvl(config.formInline,false)
        this.formWidth = config.formWidth
    }
    checkToken(config){
        JsAsse.isNotEmpty(config,"配置项config不可为空");
        if(!commons.isVoid(config.params)){
            JsAsse.isFun(config.params, '配置项config.params必须为函数')
        }
       if(config.isTree === true){
           JsAsse.isNotEmpty(config.rowKey, '配置项config.isTree = true 时config.rowKey不可为空')
       }
    }
    getArray(array){
        return Array.isArray(array) ? array : []
    }
    delete(btn,data,callback){
       let {url , method , id} = btn
       JsAsse.isTrue(!commons.isVoid(id),"未配置id") ;
       return http({
            url: url,
            method: method,
            data: [data[id] ]
        })
    }
    request(btn,data){
        let {url , method } = btn
        return http({
            url: url,
            method: method,
            data: data
        })
    }
    handlerBtn(btn,data,fun){
        if(commons.isFun(btn.callback)){
            btn.callback(data)
        }else{
            fun(data)
        }
    }
}