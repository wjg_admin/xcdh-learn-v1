import commons from "@/util/commons";

const COL = {
    index:{
        type: 'index',
        title: '序号',
        width: 80,
        align: 'center'
    }
}

export default {
    buildTable(columns = [] , Render,vue){
        this.buildCol(vue ,columns)
        let newCols = columns.filter(col=>{
            return commons.isFun(col.show) ?col.show() : commons.NVL(col.show, true)
        })
        this.buildRender(newCols , Render) ;
        return newCols ;
    },
    buildRender(columns , Render){
        columns.forEach((col)=>{
            if(Render[col.type]){
                col.render =  Render[col.type](col)
            }
        })
    },
    buildCol(vue,columns){
       let index = columns.findIndex(item=>item.type === "index") ;
       if(index === -1 && vue.showIndex){
           columns.unshift(COL.index)
       }
    }
}
