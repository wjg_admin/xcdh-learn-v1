/**
 * 全局配置文件
 */
export default {
  title: "星辰大海",
  adminHone: {
    name: "首页",
    path: "/admin/home",
    icon: 'el-icon-house' ,
  },
  clientId: 'saber',
  clientSecret: 'saber_secret',
  key: 'XCDH' ,
  pageSize: 20
}
