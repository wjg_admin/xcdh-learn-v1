import Service from "api/Service";

export default {
    dictItemColumns:[
        {
            key: 'dictType' ,
            title: "字典类型",
            required: true ,
            disabled: true
        },
        {
            title: '字典名称' ,
            key: 'dictTitle',
            required: true
        },
        {
            key: 'dictKey' ,
            title: "字典键" ,
            required: true
        },
        {
            key: 'dictVal',
            required: true ,
            title: '字典值'
        },
        {
            title: '操作',
            type: 'buttons',
            buttons:[
                {
                    type: 'update',
                    url: `/${Service.itToolService}/toolDictItem/update`,
                    method: 'POST' ,
                    title: '修改'
                },
                {
                    type: 'delete',
                    method: 'POST' ,
                    id: "dictItemId",
                    url: `/${Service.itToolService}/toolDictItem/deleteByIds`,
                    title: '删除'
                },
            ]
        }
    ]
}