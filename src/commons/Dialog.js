export default {
    data(){
      return {
      }
    },
    computed:{
        visible:{
            get(){
                return this.value ;
            },
            set(val){
                this.$emit("input",val)
            }
        },
    },
    methods:{
        dialogClose(){
            this.$emit("input",false);
        }
    }
}