const path = require('path');//引入path模块
const gwt = "http://127.0.0.1:8000/" ;
function resolve(dir){
    return path.join(__dirname,dir)//path.join(__dirname)设置绝对路径
}


module.exports={
    devServer: {
        //publicPath:"/",
      // host: '192.168.1.8',//target host
      // port: 8080,
        proxy: {
            '/learn': {
                target: gwt,
                ws: false, // 需要websocket 开启
                changOrigin: true,  //允许跨域
                pathRewrite: {
                    '^/learn': 'xcdhLearnService'
                }
            },
            '/itTool': {
                target: gwt,
                ws: false, // 需要websocket 开启
                changOrigin: true,  //允许跨域
                pathRewrite: {
                    '^/itTool': 'xcdhItTools'
                }
            },
            '/xcdhService': {
                target: gwt,
                ws: false, // 需要websocket 开启
                changOrigin: true,  //允许跨域
                pathRewrite: {
                    '^/xcdhService': 'xcdhAuth'
                }
            },
            "/api":{
                target: gwt,
                ws: false, // 需要websocket 开启
                changOrigin: true,  //允许跨域
                pathRewrite: {
                    '^/api': ''
                }
            },
            "/SD":{
                target: "http://127.0.0.1:7860/",
                ws: false, // 需要websocket 开启
                changOrigin: true,  //允许跨域
                pathRewrite: {
                    '^/SD': ''
                }
            },
            "/gpt":{
                target: "https://ai-20230803.fakeopen.com/",
                ws: false, // 需要websocket 开启
                changOrigin: true,  //允许跨域
                pathRewrite: {
                    '^/gpt': ''
                }
            }
        }
    },
    chainWebpack:(config)=>{
        config.resolve.alias
        //set第一个参数：设置的别名，第二个参数：设置的路径
        .set('@',resolve('./src'))
        .set('public',resolve('./public'))
        .set('components',resolve('./src/components'))
        .set('assets',resolve('./src/assets'))
        .set('views',resolve('./src/views'))
        .set('network',resolve('./src/network'))
        .set('api',resolve('./src/api'))
        .set('utils',resolve('./src/utils'))
    }
}
